<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package sparkle
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$container = get_theme_mod( 'sparkle_container_type' );
?>

<footer id="main">
<header>
      <a href="https://www.asianwholefoods.com/" class="logo"><img src="<?php echo get_template_directory_uri() ?>/logo-light.svg"></a>
        <div class="links">
          <a href="https://www.asianwholefoods.com/">Home</a>
          <a href="https://www.asianwholefoods.com/about/">About</a>
          <a href="<?php echo get_home_url(); ?>">Shop</a>
          <a href="https://www.asianwholefoods.com/contact/">Contact</a>
        </div>
</header>
  <div class="copyright"> Asian Whole Foods &copy; <span class="year"></span></div>
</footer>

</div><!-- #page we need this extra closing tag here -->

<?php wp_footer(); ?>

<script type="text/javascript">
  let currentYear = new Date().getFullYear();
  let el = document.querySelector(".year");
  el.innerHTML = currentYear;
</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-131687022-8"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-131687022-8');
</script>

</body>

</html>

