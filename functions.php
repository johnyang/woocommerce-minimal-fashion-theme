<?php
/**
 * sparkle functions and definitions
 *
 * @package sparkle
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$sparkle_includes = array(
	'/theme-settings.php',                  // Initialize theme default settings.
	'/setup.php',                           // Theme setup and custom theme supports.
	'/widgets.php',                         // Register widget area.
	'/enqueue.php',                         // Enqueue scripts and styles.
	'/template-tags.php',                   // Custom template tags for this theme.
	'/pagination.php',                      // Custom pagination for this theme.
	'/hooks.php',                           // Custom hooks.
	'/extras.php',                          // Custom functions that act independently of the theme templates.
	'/customizer.php',                      // Customizer additions.
	'/custom-comments.php',                 // Custom Comments file.
	'/jetpack.php',                         // Load Jetpack compatibility file.
	'/class-wp-bootstrap-navwalker.php',    // Load custom WordPress nav walker.
	'/woocommerce.php',                     // Load WooCommerce functions.
	'/editor.php',                          // Load Editor functions.
);

foreach ( $sparkle_includes as $file ) {
	$filepath = locate_template( '/inc' . $file );
	if ( ! $filepath ) {
		trigger_error( sprintf( 'Error locating /inc%s for inclusion', $file ), E_USER_ERROR );
	}
	require_once $filepath;
}

// custom function to update the read more button on article excerpt

/*
function sparkle_all_excerpts_get_more_link( $post_excerpt ) {

	return $post_excerpt . '<a class="sparkle-read-more-link" href="#">read more</a>';
}
add_filter( 'wp_trim_excerpt', 'sparkle_all_excerpts_get_more_link' );

*/

add_filter( 'woocommerce_states', 'custom_woocommerce_states' );
function custom_woocommerce_states( $states ) {
  $states['US'] = array(
		'AL' => __( 'Alabama', 'woocommerce' ),
		//'AK' => __( 'Alaska', 'woocommerce' ),
		'AZ' => __( 'Arizona', 'woocommerce' ),
		'AR' => __( 'Arkansas', 'woocommerce' ),
		'CA' => __( 'California', 'woocommerce' ),
		'CO' => __( 'Colorado', 'woocommerce' ),
		'CT' => __( 'Connecticut', 'woocommerce' ),
		'DE' => __( 'Delaware', 'woocommerce' ),
		'DC' => __( 'District Of Columbia', 'woocommerce' ),
		//'FL' => __( 'Florida', 'woocommerce' ),
		'GA' => _x( 'Georgia', 'US state of Georgia', 'woocommerce' ),
		//'HI' => __( 'Hawaii', 'woocommerce' ),
		'ID' => __( 'Idaho', 'woocommerce' ),
		'IL' => __( 'Illinois', 'woocommerce' ),
		'IN' => __( 'Indiana', 'woocommerce' ),
		'IA' => __( 'Iowa', 'woocommerce' ),
		'KS' => __( 'Kansas', 'woocommerce' ),
		'KY' => __( 'Kentucky', 'woocommerce' ),
		'LA' => __( 'Louisiana', 'woocommerce' ),
		'ME' => __( 'Maine', 'woocommerce' ),
		'MD' => __( 'Maryland', 'woocommerce' ),
		'MA' => __( 'Massachusetts', 'woocommerce' ),
		'MI' => __( 'Michigan', 'woocommerce' ),
		'MN' => __( 'Minnesota', 'woocommerce' ),
		'MS' => __( 'Mississippi', 'woocommerce' ),
		'MO' => __( 'Missouri', 'woocommerce' ),
		'MT' => __( 'Montana', 'woocommerce' ),
		'NE' => __( 'Nebraska', 'woocommerce' ),
		'NV' => __( 'Nevada', 'woocommerce' ),
		'NH' => __( 'New Hampshire', 'woocommerce' ),
		'NJ' => __( 'New Jersey', 'woocommerce' ),
		'NM' => __( 'New Mexico', 'woocommerce' ),
		'NY' => __( 'New York', 'woocommerce' ),
		'NC' => __( 'North Carolina', 'woocommerce' ),
		'ND' => __( 'North Dakota', 'woocommerce' ),
		'OH' => __( 'Ohio', 'woocommerce' ),
		'OK' => __( 'Oklahoma', 'woocommerce' ),
		'OR' => __( 'Oregon', 'woocommerce' ),
		'PA' => __( 'Pennsylvania', 'woocommerce' ),
		'RI' => __( 'Rhode Island', 'woocommerce' ),
		'SC' => __( 'South Carolina', 'woocommerce' ),
		'SD' => __( 'South Dakota', 'woocommerce' ),
		'TN' => __( 'Tennessee', 'woocommerce' ),
		'TX' => __( 'Texas', 'woocommerce' ),
		'UT' => __( 'Utah', 'woocommerce' ),
		'VT' => __( 'Vermont', 'woocommerce' ),
		'VA' => __( 'Virginia', 'woocommerce' ),
		'WA' => __( 'Washington', 'woocommerce' ),
		'WV' => __( 'West Virginia', 'woocommerce' ),
		'WI' => __( 'Wisconsin', 'woocommerce' ),
		'WY' => __( 'Wyoming', 'woocommerce' ),
		//'AA' => __( 'Armed Forces (AA)', 'woocommerce' ),
		//'AE' => __( 'Armed Forces (AE)', 'woocommerce' ),
		//'AP' => __( 'Armed Forces (AP)', 'woocommerce' ),
  );
  return $states;
}
