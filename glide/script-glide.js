var glide = new Glide('.glide', {
  type: 'carousel',
  perView: 1,
  startAt : 1,
  autoplay: 4000,
  breakpoints: {

    1200: {
      perView: 1,
    },
    768: {
      perView: 1,
    }
    
  }
})

glide.mount()

