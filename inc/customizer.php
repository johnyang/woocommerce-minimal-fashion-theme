<?php
/**
 * sparkle Theme Customizer
 *
 * @package sparkle
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

/**
 * Add postMessage support for site title and description for the Theme Customizer.
 *
 * @param WP_Customize_Manager $wp_customize Theme Customizer object.
 */
if ( ! function_exists( 'sparkle_customize_register' ) ) {
	/**
	 * Register basic customizer support.
	 *
	 * @param object $wp_customize Customizer reference.
	 */
	function sparkle_customize_register( $wp_customize ) {
		$wp_customize->get_setting( 'blogname' )->transport         = 'postMessage';
		$wp_customize->get_setting( 'blogdescription' )->transport  = 'postMessage';
		$wp_customize->get_setting( 'header_textcolor' )->transport = 'postMessage';
	}
}
add_action( 'customize_register', 'sparkle_customize_register' );

if ( ! function_exists( 'sparkle_theme_customize_register' ) ) {
	/**
	 * Register individual settings through customizer's API.
	 *
	 * @param WP_Customize_Manager $wp_customize Customizer reference.
	 */
	function sparkle_theme_customize_register( $wp_customize ) {
		
/****************************************************************************************************************/

		//file input sanitization function
		function sparkle_theme_slug_sanitize_file( $file, $setting ) {
			$mimes = array(
					'jpg|jpeg|jpe' => 'image/jpeg',
					'gif'          => 'image/gif',
					'png'          => 'image/png'
			);
			$file_ext = wp_check_filetype( $file, $mimes );
			return ( $file_ext['ext'] ? $file : $setting->default );
		}
		//select input sanitization function
		function sparkle_theme_slug_sanitize_select( $input, $setting ){
			$input = sanitize_key( $input );
			$choices = $setting->manager->get_control( $setting->id )->choices;
			return ( array_key_exists( $input, $choices ) ? $input : $setting->default );                
		}
		//radio input sanitization function
		function sparkle_theme_slug_sanitize_radio( $input, $setting ){
			$input = sanitize_key($input);
			$choices = $setting->manager->get_control( $setting->id )->choices;
			return ( array_key_exists( $input, $choices ) ? $input : $setting->default );                				
		}


/**************************************************************************************************** */

		// Slider Section
		$wp_customize->add_section( 'sparkle_slider_options', array(
			'title'       => __( 'Slider Settings', 'sparkle' ),
			'capability'  => 'edit_theme_options',
			'description' => __( 'Slider settings', 'sparkle' ),
			'priority'    => 160,
		) );

		$wp_customize->add_setting( 'sparkle_slider_count', array(
			'type'              => 'theme_mod',
			'sanitize_callback' => 'sparkle_theme_slug_sanitize_radio',
			'capability'        => 'edit_theme_options',
		) );

		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'sparkle_slider_count', array(
					'label'       => __( 'Slide Count', 'sparkle' ),
					'description' => __( 'Choose the # of Slides to Show', 'sparkle' ),
					'section'     => 'sparkle_slider_options',
					'settings'    => 'sparkle_slider_count',
					'type'        => 'radio',
					'choices'     => array('one', 'two', 'three' ),
					'priority'    => '10',
				)
			) 
		);


		$wp_customize->add_setting('sparkle_slider_1_image', array(
			'type' => 'theme_mod',
			'sanitize_callback' => 'sparkle_theme_slug_sanitize_file',
			'capability' => 'edit_theme_options',
		));

		$wp_customize->add_control(
			new WP_Customize_Image_Control(
				$wp_customize,
				'sparkle_slider_1_image', array(
					'label' => __( 'Slider #1 Image', 'sparkle'),
					'section' => 'sparkle_slider_options',
					'settings' => 'sparkle_slider_1_image',
					'priority' => '20',
				)
			)
		);

		$wp_customize->add_setting('sparkle_slider_1_headline', array(
			'type' => 'theme_mod',
			'capability' => 'edit_theme_options',
		));

		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'sparkle_slider_1_headline', array(
					'label' => __( 'Slider #1 Headline', 'sparkle'),
					'description' => __( ' Slider #1 Headline '),
					'section' => 'sparkle_slider_options',
					'settings' => 'sparkle_slider_1_headline',
					'type' => 'text',
					'priority' => '25',
				)
			)
		);

		$wp_customize->add_setting('sparkle_slider_1_excerpt', array(
			'type' => 'theme_mod',
			'capability' => 'edit_theme_options',
		));

		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'sparkle_slider_1_excerpt', array(
					'label' => __( 'Slider #1 Excerpt', 'sparkle'),
					'description' => __( ' Slider #1 Excerpt '),
					'section' => 'sparkle_slider_options',
					'settings' => 'sparkle_slider_1_excerpt',
					'type' => 'textarea',
					'priority' => '30',
				)
			)
		);


		$wp_customize->add_setting('sparkle_slider_2_image', array(
			'type' => 'theme_mod',
			'sanitize_callback' => 'sparkle_theme_slug_sanitize_file',
			'capability' => 'edit_theme_options',
		));

		$wp_customize->add_control(
			new WP_Customize_Image_Control(
				$wp_customize,
				'sparkle_slider_2_image', array(
					'label' => __( 'Slider #2 Image', 'sparkle'),
					'section' => 'sparkle_slider_options',
					'settings' => 'sparkle_slider_2_image',
					'priority' => '35',
				)
			)
		);


		$wp_customize->add_setting('sparkle_slider_2_headline', array(
			'type' => 'theme_mod',
			'capability' => 'edit_theme_options',
		));

		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'sparkle_slider_2_headline', array(
					'label' => __( 'Slider #2 Headline', 'sparkle'),
					'description' => __( ' Slider #2 Headline '),
					'section' => 'sparkle_slider_options',
					'settings' => 'sparkle_slider_2_headline',
					'type' => 'text',
					'priority' => '40',
				)
			)
		);

		$wp_customize->add_setting('sparkle_slider_2_excerpt', array(
			'type' => 'theme_mod',
			'capability' => 'edit_theme_options',
		));

		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'sparkle_slider_2_excerpt', array(
					'label' => __( 'Slider #1 Excerpt', 'sparkle'),
					'description' => __( ' Slider #2 Excerpt '),
					'section' => 'sparkle_slider_options',
					'settings' => 'sparkle_slider_2_excerpt',
					'type' => 'textarea',
					'priority' => '45',
				)
			)
		);


		$wp_customize->add_setting('sparkle_slider_3_image', array(
			'type' => 'theme_mod',
			'sanitize_callback' => 'sparkle_theme_slug_sanitize_file',
			'capability' => 'edit_theme_options',
		));

		$wp_customize->add_control(
			new WP_Customize_Image_Control(
				$wp_customize,
				'sparkle_slider_3_image', array(
					'label' => __( 'Slider #3 Image', 'sparkle'),
					'section' => 'sparkle_slider_options',
					'settings' => 'sparkle_slider_3_image',
					'priority' => '50',
				)
			)
		);

		$wp_customize->add_setting('sparkle_slider_3_headline', array(
			'type' => 'theme_mod',
			'capability' => 'edit_theme_options',
		));

		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'sparkle_slider_3_headline', array(
					'label' => __( 'Slider #3 Headline', 'sparkle'),
					'description' => __( ' Slider #3 Headline '),
					'section' => 'sparkle_slider_options',
					'settings' => 'sparkle_slider_3_headline',
					'type' => 'text',
					'priority' => '55',
				)
			)
		);

		$wp_customize->add_setting('sparkle_slider_3_excerpt', array(
			'type' => 'theme_mod',
			'capability' => 'edit_theme_options',
		));

		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'sparkle_slider_3_excerpt', array(
					'label' => __( 'Slider #3 Excerpt', 'sparkle'),
					'description' => __( ' Slider #3 Excerpt '),
					'section' => 'sparkle_slider_options',
					'settings' => 'sparkle_slider_3_excerpt',
					'type' => 'textarea',
					'priority' => '60',
				)
			)
		);


/*******************************************************************************************************************/

		// Newsletter Section
		$wp_customize->add_section( 'sparkle_newsletter_options', array(
			'title'       => __( 'Newsletter', 'sparkle' ),
			'capability'  => 'edit_theme_options',
			'description' => __( 'Newsletter settings', 'sparkle' ),
			'priority'    => 170,
		) );

		$wp_customize->add_setting('sparkle_newsletter_code', array(
			'type' => 'theme_mod',
			'capability' => 'edit_theme_options',
		));

		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'sparkle_newsletter_code', array(
					'label' => __( 'Newsletter Code', 'sparkle'),
					'description' => __( ' Newsletter code goes here '),
					'section' => 'sparkle_newsletter_options',
					'settings' => 'sparkle_newsletter_code',
					'type' => 'textarea',
					'priority' => '20',
				)
			)
		);

/**************************************************************************************************************** */


		// Color Section
		$wp_customize->add_section( 'sparkle_color_options', array(
			'title'       => __( 'CTA Button Colors', 'sparkle' ),
			'capability'  => 'edit_theme_options',
			'description' => __( 'Color settings', 'sparkle' ),
			'priority'    => 180,
		) );

		$wp_customize->add_setting( 
			'sparkle_cta_button_color', 
			array(
					'default' => '#000000',
					'sanitize_callback' => 'sanitize_hex_color' //validates 3 or 6 digit HTML hex color code
			)
		);
		
		$wp_customize->add_control( 
				new WP_Customize_Color_Control( 
				$wp_customize, 
				'sparkle_cta_button_color', 
						array(              
								'label'      => __( 'CTA Button Color', 'sparkle' ),
								'section'    => 'sparkle_color_options'        
						)
				) 
		); 

		$wp_customize->add_setting( 
			'sparkle_cta_button_color_text', 
			array(
					'default' => '#000000',
					'sanitize_callback' => 'sanitize_hex_color' //validates 3 or 6 digit HTML hex color code
			)
		);
		
		$wp_customize->add_control( 
				new WP_Customize_Color_Control( 
				$wp_customize, 
				'sparkle_cta_button_color_text', 
						array(              
								'label'      => __( 'CTA Button Color Text', 'sparkle' ),
								'section'    => 'sparkle_color_options'        
						)
				) 
		); 

		$wp_customize->add_setting( 
			'sparkle_cta_button_hover_color', 
			array(
					'default' => '#000000',
					'sanitize_callback' => 'sanitize_hex_color' //validates 3 or 6 digit HTML hex color code
			)
		);
		
		$wp_customize->add_control( 
				new WP_Customize_Color_Control( 
				$wp_customize, 
				'sparkle_cta_button_hover_color', 
						array(              
								'label'      => __( 'CTA Button Hover Color', 'sparkle' ),
								'section'    => 'sparkle_color_options'        
						)
				) 
		); 

		$wp_customize->add_setting( 
			'sparkle_cta_button_hover_color_text', 
			array(
					'default' => '#000000',
					'sanitize_callback' => 'sanitize_hex_color' //validates 3 or 6 digit HTML hex color code
			)
		);
		
		$wp_customize->add_control( 
				new WP_Customize_Color_Control( 
				$wp_customize, 
				'sparkle_cta_button_hover_color_text', 
						array(              
								'label'      => __( 'CTA Hover Color Text', 'sparkle' ),
								'section'    => 'sparkle_color_options'        
						)
				) 
		); 

/**************************************************************************************************************** */


		// Slider Section
		$wp_customize->add_section( 'sparkle_featured_options', array(
			'title'       => __( 'Featured Section', 'sparkle' ),
			'capability'  => 'edit_theme_options',
			'description' => __( 'Homepage featured section options', 'sparkle' ),
			'priority'    => 160,
		) );

		$wp_customize->add_setting('sparkle_featured_1_image', array(
			'type' => 'theme_mod',
			'sanitize_callback' => 'sparkle_theme_slug_sanitize_file',
			'capability' => 'edit_theme_options',
		));

		$wp_customize->add_control(
			new WP_Customize_Image_Control(
				$wp_customize,
				'sparkle_featured_1_image', array(
					'label' => __( 'featured #1 Image', 'sparkle'),
					'section' => 'sparkle_featured_options',
					'settings' => 'sparkle_featured_1_image',
					'priority' => '20',
				)
			)
		);

		$wp_customize->add_setting('sparkle_featured_1_headline', array(
			'type' => 'theme_mod',
			'capability' => 'edit_theme_options',
		));

		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'sparkle_featured_1_headline', array(
					'label' => __( 'featured #1 Headline', 'sparkle'),
					'description' => __( ' featured #1 Headline '),
					'section' => 'sparkle_featured_options',
					'settings' => 'sparkle_featured_1_headline',
					'type' => 'text',
					'priority' => '25',
				)
			)
		);

		$wp_customize->add_setting('sparkle_featured_1_excerpt', array(
			'type' => 'theme_mod',
			'capability' => 'edit_theme_options',
		));

		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'sparkle_featured_1_excerpt', array(
					'label' => __( 'featured #1 Excerpt', 'sparkle'),
					'description' => __( ' featured #1 Excerpt '),
					'section' => 'sparkle_featured_options',
					'settings' => 'sparkle_featured_1_excerpt',
					'type' => 'textarea',
					'priority' => '30',
				)
			)
		);

		$wp_customize->add_setting('sparkle_featured_2_image', array(
			'type' => 'theme_mod',
			'sanitize_callback' => 'sparkle_theme_slug_sanitize_file',
			'capability' => 'edit_theme_options',
		));

		$wp_customize->add_control(
			new WP_Customize_Image_Control(
				$wp_customize,
				'sparkle_featured_2_image', array(
					'label' => __( 'featured #2 Image', 'sparkle'),
					'section' => 'sparkle_featured_options',
					'settings' => 'sparkle_featured_2_image',
					'priority' => '35',
				)
			)
		);

		$wp_customize->add_setting('sparkle_featured_2_headline', array(
			'type' => 'theme_mod',
			'capability' => 'edit_theme_options',
		));

		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'sparkle_featured_2_headline', array(
					'label' => __( 'featured #2 Headline', 'sparkle'),
					'description' => __( ' featured #2 Headline '),
					'section' => 'sparkle_featured_options',
					'settings' => 'sparkle_featured_2_headline',
					'type' => 'text',
					'priority' => '40',
				)
			)
		);

		$wp_customize->add_setting('sparkle_featured_2_excerpt', array(
			'type' => 'theme_mod',
			'capability' => 'edit_theme_options',
		));

		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'sparkle_featured_2_excerpt', array(
					'label' => __( 'featured #1 Excerpt', 'sparkle'),
					'description' => __( ' featured #2 Excerpt '),
					'section' => 'sparkle_featured_options',
					'settings' => 'sparkle_featured_2_excerpt',
					'type' => 'textarea',
					'priority' => '45',
				)
			)
		);

		$wp_customize->add_setting('sparkle_featured_3_image', array(
			'type' => 'theme_mod',
			'sanitize_callback' => 'sparkle_theme_slug_sanitize_file',
			'capability' => 'edit_theme_options',
		));

		$wp_customize->add_control(
			new WP_Customize_Image_Control(
				$wp_customize,
				'sparkle_featured_3_image', array(
					'label' => __( 'featured #3 Image', 'sparkle'),
					'section' => 'sparkle_featured_options',
					'settings' => 'sparkle_featured_3_image',
					'priority' => '50',
				)
			)
		);

		$wp_customize->add_setting('sparkle_featured_3_headline', array(
			'type' => 'theme_mod',
			'capability' => 'edit_theme_options',
		));

		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'sparkle_featured_3_headline', array(
					'label' => __( 'featured #3 Headline', 'sparkle'),
					'description' => __( ' featured #3 Headline '),
					'section' => 'sparkle_featured_options',
					'settings' => 'sparkle_featured_3_headline',
					'type' => 'text',
					'priority' => '55',
				)
			)
		);

		$wp_customize->add_setting('sparkle_featured_3_excerpt', array(
			'type' => 'theme_mod',
			'capability' => 'edit_theme_options',
		));

		$wp_customize->add_control(
			new WP_Customize_Control(
				$wp_customize,
				'sparkle_featured_3_excerpt', array(
					'label' => __( 'featured #3 Excerpt', 'sparkle'),
					'description' => __( ' featured #3 Excerpt '),
					'section' => 'sparkle_featured_options',
					'settings' => 'sparkle_featured_3_excerpt',
					'type' => 'textarea',
					'priority' => '60',
				)
			)
		);

/**************************************************************************************************************** */

	}
} 
add_action( 'customize_register', 'sparkle_theme_customize_register' );

// js preview
if ( ! function_exists( 'sparkle_customize_preview_js' ) ) {
	function sparkle_customize_preview_js() {
		wp_enqueue_script( 'sparkle_customizer', get_template_directory_uri() . '/js/customizer.js',
			array( 'customize-preview' ), '20130508', true
		);
	}
}
add_action( 'customize_preview_init', 'sparkle_customize_preview_js' );


add_action( 'wp_head', 'cta_button_css');
function cta_button_css()
{
    ?>
         <style type="text/css">
						 a.cta.button { 
							 background: <?php echo get_theme_mod('sparkle_cta_button_color', '#43C6E4'); ?>; 
							 color: <?php echo get_theme_mod('sparkle_cta_button_hover_color_text', '#43C6E4'); ?>;
							}
						 a.cta.button:hover { 
							 background: <?php echo get_theme_mod('sparkle_cta_button_hover_color', '#43C6E4'); ?>;
							 color: <?php echo get_theme_mod('sparkle_cta_button_hover_color_text', '#43C6E4'); ?>
							}
         </style>
    <?php
}

function sparkle_customize_register( $wp_customize ) {
	//All our sections, settings, and controls will be added here

	//$wp_customize->remove_section( 'title_tagline');
	$wp_customize->remove_section( 'colors');
	$wp_customize->remove_section( 'header_image');
	$wp_customize->remove_section( 'background_image');
	$wp_customize->remove_section( 'menus');
	$wp_customize->remove_section( 'static_front_page');
	$wp_customize->remove_section( 'custom_css');

}

add_action( 'customize_register', 'sparkle_customize_register' );