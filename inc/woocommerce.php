<?php
/**
 * Add WooCommerce support
 *
 * @package sparkle
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

add_action( 'after_setup_theme', 'sparkle_woocommerce_support' );
if ( ! function_exists( 'sparkle_woocommerce_support' ) ) {
	/**
	 * Declares WooCommerce theme support.
	 */
	function sparkle_woocommerce_support() {
		add_theme_support( 'woocommerce' );

		// Add New Woocommerce 3.0.0 Product Gallery support
		add_theme_support( 'wc-product-gallery-lightbox' );
		add_theme_support( 'wc-product-gallery-zoom' );
		add_theme_support( 'wc-product-gallery-slider' );

		// hook in and customizer form fields.
		add_filter( 'woocommerce_form_field_args', 'sparkle_wc_form_field_args', 10, 3 );
	}
}

/**
* First unhook the WooCommerce wrappers
*/
remove_action( 'woocommerce_before_main_content', 'woocommerce_output_content_wrapper', 10);
remove_action( 'woocommerce_after_main_content', 'woocommerce_output_content_wrapper_end', 10);

/**
* Then hook in your own functions to display the wrappers your theme requires
*/
add_action('woocommerce_before_main_content', 'sparkle_woocommerce_wrapper_start', 10);
add_action('woocommerce_after_main_content', 'sparkle_woocommerce_wrapper_end', 10);

if ( ! function_exists( 'sparkle_woocommerce_wrapper_start' ) ) {
	function sparkle_woocommerce_wrapper_start() {
		$container   = get_theme_mod( 'sparkle_container_type' );
		echo '<div class="wrapper" id="wrapper-woocommerce">';
	  echo '<div class="' . esc_attr( $container ) . '" id="content" tabindex="-1">';
		//echo '<div class="row">';
		//get_template_part( 'global-templates/left-sidebar-check' );
		echo '<main class="site-main" id="main">';
	}
}
if ( ! function_exists( 'sparkle_woocommerce_wrapper_end' ) ) {
function sparkle_woocommerce_wrapper_end() {
	echo '</main><!-- #main -->';
	//get_template_part( 'global-templates/right-sidebar-check' );
  //echo '</div><!-- .row -->';
	echo '</div><!-- Container end -->';
	echo '</div><!-- Wrapper end -->';
	}
}


/**
 * Filter hook function monkey patching form classes
 * Author: Adriano Monecchi http://stackoverflow.com/a/36724593/307826
 *
 * @param string $args Form attributes.
 * @param string $key Not in use.
 * @param null   $value Not in use.
 *
 * @return mixed
 */
if ( ! function_exists ( 'sparkle_wc_form_field_args' ) ) {
	function sparkle_wc_form_field_args( $args, $key, $value = null ) {
		// Start field type switch case.
		switch ( $args['type'] ) {
			/* Targets all select input type elements, except the country and state select input types */
			case 'select' :
				// Add a class to the field's html element wrapper - woocommerce
				// input types (fields) are often wrapped within a <p></p> tag.
				$args['class'][] = 'form-group';
				// Add a class to the form input itself.
				$args['input_class']       = array( 'form-control', 'input-lg' );
				$args['label_class']       = array( 'control-label' );
				$args['custom_attributes'] = array(
					'data-plugin'      => 'select2',
					'data-allow-clear' => 'true',
					'aria-hidden'      => 'true',
					// Add custom data attributes to the form input itself.
				);
				break;
			// By default WooCommerce will populate a select with the country names - $args
			// defined for this specific input type targets only the country select element.
			case 'country' :
				$args['class'][]     = 'form-group single-country';
				$args['label_class'] = array( 'control-label' );
				break;
			// By default WooCommerce will populate a select with state names - $args defined
			// for this specific input type targets only the country select element.
			case 'state' :
				// Add class to the field's html element wrapper.
				$args['class'][] = 'form-group';
				// add class to the form input itself.
				$args['input_class']       = array( '', 'input-lg' );
				$args['label_class']       = array( 'control-label' );
				$args['custom_attributes'] = array(
					'data-plugin'      => 'select2',
					'data-allow-clear' => 'true',
					'aria-hidden'      => 'true',
				);
				break;
			case 'password' :
			case 'text' :
			case 'email' :
			case 'tel' :
			case 'number' :
				$args['class'][]     = 'form-group';
				$args['input_class'] = array( 'form-control', 'input-lg' );
				$args['label_class'] = array( 'control-label' );
				break;
			case 'textarea' :
				$args['input_class'] = array( 'form-control', 'input-lg' );
				$args['label_class'] = array( 'control-label' );
				break;
			case 'checkbox' :
				$args['label_class'] = array( 'custom-control custom-checkbox' );
				$args['input_class'] = array( 'custom-control-input', 'input-lg' );
				break;
			case 'radio' :
				$args['label_class'] = array( 'custom-control custom-radio' );
				$args['input_class'] = array( 'custom-control-input', 'input-lg' );
				break;
			default :
				$args['class'][]     = 'form-group';
				$args['input_class'] = array( 'form-control', 'input-lg' );
				$args['label_class'] = array( 'control-label' );
				break;
		} // end switch ($args).
		return $args;
	}
}

// remove the add to cart button in the product loop
remove_action('woocommerce_after_shop_loop_item', 'woocommerce_template_loop_add_to_cart', 10);


remove_action('woocommerce_before_main_content','woocommerce_breadcrumb', 20);
add_action('woocommerce_before_main_content','custom_woocommerce_breadcrumb', 20);
	function custom_woocommerce_breadcrumb( $args = array() ) {
		$args = wp_parse_args( $args, apply_filters( 'woocommerce_breadcrumb_defaults', array(
			'delimiter'   => '&nbsp;&#47;&nbsp;',
			'wrap_before' => '<nav class="woocommerce-breadcrumb">',
			'wrap_after'  => '</nav>',
			'before'      => '',
			'after'       => '',
			'home'        => _x( 'Home', 'breadcrumb', 'woocommerce' ),
		) ) );

		$breadcrumbs = new WC_Breadcrumb();

		if ( ! empty( $args['home'] ) ) {
			$breadcrumbs->add_crumb( $args['home'], apply_filters( 'woocommerce_breadcrumb_home_url', home_url() ) );
		}

		$args['breadcrumb'] = $breadcrumbs->generate();

		/**
		 * WooCommerce Breadcrumb hook
		 *
		 * @hooked WC_Structured_Data::generate_breadcrumblist_data() - 10
		 */
		do_action( 'woocommerce_breadcrumb', $breadcrumbs, $args );

		wc_get_template( 'global/breadcrumb.php', $args );
	}
	
	remove_action('woocommerce_before_shop_loop','woocommerce_output_all_notices', 10);
	add_action('woocommerce_before_shop_loop','custom_woocommerce_output_all_notices', 10);
	function custom_woocommerce_output_all_notices() {
		echo '<div class="woocommerce-notices-wrapper">';
		wc_print_notices();
		echo '</div>';
	}

	/*
	hook: woocommerce_before_shop_loop
	@hooked woocommerce_result_count - 20
	@hooked woocommerce_catalog_ordering - 30
	*/

	remove_action('woocommerce_before_shop_loop','woocommerce_result_count', 20);
	add_action('woocommerce_after_shop_loop', 'woocommerce_result_count', 30);

	remove_action('woocommerce_before_shop_loop','woocommerce_catalog_ordering',30);
	add_action('woocommerce_after_shop_loop','woocommerce_catalog_ordering',20);

	/*
 	hook: woocommerce_after_shop_loop
 	@hooked woocommerce_pagination - 10
	*/

	remove_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 5);
	add_action('woocommerce_after_shop_loop_item_title', 'woocommerce_template_loop_rating', 20);

	/*
	woocommerce_template_loop_rating - 5
	woocommerce_after_shop_loop_item_title
	*/


	/**
 * Rename product data tabs
 */
add_filter( 'woocommerce_product_tabs', 'woo_rename_tabs', 98 );
function woo_rename_tabs( $tabs ) {

	$tabs['description']['title'] = __( 'Description' );		// Rename the description tab
	$tabs['reviews']['title'] = __( 'Reviews' );				// Rename the reviews tab
	$tabs['additional_information']['title'] = __( 'Details' );	// Rename the additional information tab

	return $tabs;

}