<?php
/**
 * sparkle enqueue scripts
 *
 * @package sparkle
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( ! function_exists( 'sparkle_scripts' ) ) {
	/**
	 * Load theme's JavaScript and CSS sources.
	 */
	function sparkle_scripts() {
		// Get the theme data.
		$the_theme = wp_get_theme();
		$theme_version = $the_theme->get( 'Version' );
	
		wp_enqueue_style( 'sparkle-styles', get_stylesheet_directory_uri() . '/dist/theme.min.css', array(), $css_version );
		wp_enqueue_script( 'sparkle-scripts', get_template_directory_uri() . '/dist/custom-min.js', array(), $js_version, true );
		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}
	}
} // endif function_exists( 'sparkle_scripts' ).

add_action( 'wp_enqueue_scripts', 'sparkle_scripts' );