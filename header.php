<?php
/**
 * The header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="content">
 *
 * @package sparkle
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
$container   = get_theme_mod( 'sparkle_container_type' );

?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-title" content="<?php bloginfo( 'name' ); ?> - <?php bloginfo( 'description' ); ?>">
	<link rel="profile" href="http://gmpg.org/xfn/11">
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">
	<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>

<div class="site" id="page">

	<div id="navbar-wrapper" itemscope itemtype="http://schema.org/WebSite">

	<nav id="primary">

  <div id="logo">
    <a href="https://www.asianwholefoods.com/"><img src="<?php echo get_template_directory_uri() ?>/logo-dark.svg"></a>
  </div>

  <button onClick="showMobileMenu()" id="mobileMenuBurgerButton" class="hamburger hamburger--boring" type="button">
    <span class="hamburger-box">
      <span class="hamburger-inner"></span>
    </span>
  </button>

  <?php wp_nav_menu(
						array(
							'theme_location'  => 'primary',
							'container_class' => '',
							'container_id'    => 'primaryMenuWrapper',
							'menu_class'      => '',
							'fallback_cb'     => '',
							'menu_id'         => 'primary_menu',
							'depth'           => 1,
							'walker'          => new sparkle_WP_Bootstrap_Navwalker(),
						)
					); ?>
</nav>
	</div><!-- #wrapper-navbar end -->




