<?php
/**
 * Single Product tabs
 *
 * Modified from original woocommerce tab template by replacing tabs w/ an accordion.  
 * 
 * @see 	https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Filter tabs and allow third parties to add their own.
 *
 * Each tab is an array containing title, callback and priority.
 * @see woocommerce_default_product_tabs()
 */
$tabs = apply_filters( 'woocommerce_product_tabs', array() );

if ( ! empty( $tabs ) ) : ?>

	<div id="single_product_accordion" class="woocommerce-tabs wc-tabs-wrapper">
		<div class="" role="tablist">
			<?php foreach ( $tabs as $key => $tab ) : ?>
				<div class="" id="tab-title-<?php echo esc_attr( $key ); ?>" role="tab" aria-controls="tab-<?php echo esc_attr( $key ); ?>">
					<a data-toggle="collapse" href="#tab-<?php echo esc_attr( $key ); ?>"><?php echo apply_filters( 'woocommerce_product_' . $key . '_tab_title', esc_html( $tab['title'] ), $key ); ?></a>
				</div>
				<div class="collapse" id="tab-<?php echo esc_attr( $key ); ?>" data-parent="#single_product_accordion" role="tabpanel" aria-labelledby="tab-title-<?php echo esc_attr( $key ); ?>">
				<?php if ( isset( $tab['callback'] ) ) { call_user_func( $tab['callback'], $key, $tab ); } ?>
			</div>
			<?php endforeach; ?>
		</div>
	</div>

<?php endif; ?>
