<?php
/**
 * Single Product tabs
 *
 * Modified from original woocommerce tab template by replacing tabs w/ an accordion.  
 * 
 * @see 	https://docs.woocommerce.com/document/template-structure/
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Filter tabs and allow third parties to add their own.
 *
 * Each tab is an array containing title, callback and priority.
 * @see woocommerce_default_product_tabs()
 */

 
//wc_get_template( 'single-product/tabs/description.php' );

if(comments_open()){
	comments_template();
}